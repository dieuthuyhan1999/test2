# File: base_test.py
# Author: tran.vanthanh@tap-ic.co.jp
# Date: 04 May 2020
# Desc: A test class for others to inherit to prevent duplicate code.

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from datetime import datetime
from selenium import webdriver
from data.data_provider import get_data
from config import DEFAULT_DRIVER, BASE_URL
from time import sleep
import random
import HtmlTestRunner
import unittest
import sys
import os

class BaseTest(unittest.TestCase):
    # def __init__(self, driver, base_url, module):
    #     """Init
    #     Parameters
    #     ----------
    #     driver : object
    #         The selenium web driver
    #     base_url : string
    #         The base url of the web page we will be visiting.
    #     module
    #         The module currently being executed
    #     """
        
    #     self.driver = driver
    #     self.base_url = base_url
    #     self.module = module
    #     self.wait = WebDriverWait(driver, 10)  

    def setUp(self):
        self.driver = DEFAULT_DRIVER
        self.driver.maximize_window()
        self.base_url = BASE_URL
        self.wait = WebDriverWait(DEFAULT_DRIVER, 20)  

    def check_test_passed(self, passed=True):
        """Print a generic message when a test has passed or failed
        --------
        passed: bool
            True: all of the function in the test were passed
            False: has fail function in the test
        """
        self.assertTrue(passed)
        # if passed:
        #     print("Passed: " + self.module)
        # else:
        #     print("Failed:" + self.module)
        
    def check_function_failed(self, function_check):
        """Print a generic message when a function has failed
        """

        print("Failed: " + function_check)
        self.take_screenshot(function_check)
            
    def take_screenshot(self,module_name):
        """
        Take a screenshot with a defined name based on module and the browser
        -----
        error : string 
            Type of screenshot(error or no)
                default: module screenshot
                "error": if want to take screenshot of error function. Default is pass        
        """

        #today = date.today()
        now = datetime.now()
        #millis = int(round(time.time() * 1000))
        currentTime = now.strftime("%Y-%m-%d-%H-%M-%S")
        if(self.driver.name):
            driver_name = self.driver.name
        else:
            driver_name = ""

        self.driver.save_screenshot("screenshots/" +  driver_name + "-" + module_name + "_" + str(currentTime) + ".png")
        
if __name__ == "__main__":
    unittest.main(testRunner=HtmlTestRunner.HTMLTestRunner())
