# File: config.py
# Author: tran.vanthanh@tap-ic.co.jp
# Date: 04 May 2020

import os
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
# Base url to run test scripts
BASE_URL = 'http://localhost:18780/u0000psm/'

# FIREFOX_DRIVER_PATH = os.path.dirname(os.path.realpath(__file__)) + "\\drivers/geckodriver.exe"
# CHROME_DRIVER_PATH = os.path.dirname(os.path.realpath(__file__)) + "\\drivers/chromedriver.exe"
IMAGE_PATH = os.path.dirname(os.path.realpath(__file__)) + "\\image_product/image/burger.png"

# Default driver to run tests
options = Options()
options.add_argument("--headless")
options.add_argument("window-size=1920x1080")
options.add_argument("--disable-gpu")
options.add_argument("--no-sandbox")
options.add_argument("--disable-cache")
options.add_argument("--disable-application-cache")
DEFAULT_DRIVER = webdriver.Chrome(ChromeDriverManager().install(), options=options)

# Test Folder
TEST_BASE = os.path.dirname(os.path.realpath(__file__)) + "\\tests"
