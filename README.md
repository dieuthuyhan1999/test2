TAP WEBPOS Test Automatic With Selenium and Python
=========
This is test automatic Tap WEBPOS application, using Selenium and Python.
```
Created by: tran.vanthanh@tap-ic.co.jp

Date: 04 May 2020

Version 0.1
````

# I- Requirements
- Install Python version 3.8.x
```
https://www.python.org/downloads/release/python-382/
````
Download file "Windows x86-64 executable installer" if you're using Windows

- Install Selenium 
```
pip install selenium
````

- Install webdriver manager
```
pip install webdriver-manager
````

* Download Firefox Gecko Driver
```
https://github.com/mozilla/geckodriver/releases
````
Extract Zip file to somewhere, example: "PROJECT_PATH\drivers\geckodriver.exe"

# II- How to Run

## 1- To run all tests
```
py run.py
````

## 2- Run with custom browser
> To run with Chrome [ch]
```
py run.py -d ch
````
OR
```
py run.py --driver ch
````

> To run with Firefox [ff]
```
"py run.py -d ff"
````
 OR 
```
py run.py --driver ff
````
## 3- To only run one test
```
py run.py -t "user_login.py"
````
OR
```
py run.py --test "user_login.py"
````

## 4- Run with many arguments at the same time
```
py run.py -d "ch" -t "user_login.py"
````

## 5- To see more options
```
py run.py -h
````
OR
```
py run.py --help
````

# III- Resources

- Gitlab: 
https://gitlab.com/thv-dev-team/testauto_weppos