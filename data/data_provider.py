import os
import json

def get_data():
    """Load data test from data.json"""
    try:
        abs_path = os.path.dirname(os.path.realpath(__file__)) + "\\data.json"
        with open(abs_path, "r", encoding='utf-8', errors='ignore') as data:
            read_json_file = json.load(data)
        return read_json_file
    except:
        print(abs_path)
        print(os.path.realpath(__file__))
        raise Exception("No JSON file found!")
