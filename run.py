# File: run.py
# Author: tran.vanthanh@tap-ic.co.jp
# Date: 04 May 2020

#==========================================================================================#
# 1- Import libraries
#==========================================================================================#
from selenium import webdriver
import time
import os
import argparse
import config
import sys
from config import TEST_BASE, BASE_URL
from unittest import TestLoader, TestSuite
from HtmlTestRunner import HTMLTestRunner

#==========================================================================================#
# 2- Arguments
#==========================================================================================#
parser = argparse.ArgumentParser()
parser.add_argument("-d", "--driver", help="Running test with Chrome (ch) or Firefox (ff)")
parser.add_argument("-t", "--test", help="Only run one test as specified", default="all")
args = parser.parse_args()
# if args.driver == "ff":
#     print("------Starting test with Firefox------")
#     config.DEFAULT_DRIVER = webdriver.Firefox(executable_path=FIREFOX_DRIVER_PATH)
#==========================================================================================#
# 3- Run Test cases
#==========================================================================================#
# Check base url is empty
if (BASE_URL == ""):
    print("You need to set your BASE_URL in config.py.")
    exit()

# Check if default driver is empty
if not (hasattr(config, 'DEFAULT_DRIVER')):
    print("You need to set your DEFAULT_DRIVER in config.py.")
    exit()

# Check run all Test cases or run specified (by name of Test case file)
arr_test_cases = []
arr_tests = []

listOfFiles = list()
if(args.test == "all"):
	# if args.test == "all" then dynamically set up that list from all the file names in the test folder
    for file in [doc for doc in os.listdir(TEST_BASE) if doc.endswith(".py") and doc != "__init__.py" and doc != "base_test.py"]:	
    	arr_test_cases.append("tests." + file.split(".")[0])  # remove the .py from the test name		
else:
	# Otherwise, just add the one tests specified when passing the command
    arr_test_cases = ["tests." + args.test.split(".")[0]]  # remove the .py from the test name	

# The time when the test started
all_tests_start_time = time.time()

# Loop through all the tests_to_run and runs them
for test_to_run in arr_test_cases:
	# The time when the test started
    this_test_start_time = time.time()
    # This dynamically imports all modules in the tests_to_run list. This allows me to import a module using
    # a variable. This is fairly advanced and hard to follow for the beginner.
    current_test = getattr(__import__(test_to_run, fromlist=["Test"]), "Test")
    # # Test calls without reports
    # test = current_test(driver, BASE_URL, test_to_run) 
    # test.run()
    test = TestLoader().loadTestsFromTestCase(current_test)
    arr_tests.append(test)
	# # Output the amount of time it took this test to run on the current platform
    # this_test_seconds_taken = time.time() - this_test_start_time
    # if(this_test_seconds_taken > 60):
    #     print("Time taken: " + str(this_test_seconds_taken / 60) + " minutes")
    # else:
    #     print("Time taken: " + str(this_test_seconds_taken) + " seconds")

# Run all the tests which be collected
suite = TestSuite(arr_tests)
runner = HTMLTestRunner(combine_reports=True, report_name="WebPos_Master_Report", add_timestamp=False)
result = runner.run(suite)


# Output the amount of time it took all tests to run on the current platform
print("--------------------------------------------------\n")
all_tests_seconds_taken = (time.time() - all_tests_start_time)
if(all_tests_seconds_taken > 60):
    print("Time taken for all tests: " + str(all_tests_seconds_taken / 60) + " minutes")
else:
    print("Time taken for all tests: " + str(all_tests_seconds_taken) + " seconds")

#==========================================================================================#
# 4- Quit
#==========================================================================================#
config.DEFAULT_DRIVER.quit()

#Mark the test as failure if an any error occur
sys.exit(not result.wasSuccessful())